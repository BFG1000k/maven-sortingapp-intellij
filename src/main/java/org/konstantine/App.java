package org.konstantine;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Konstantine Savrasov
 *
 */
public class App
{
    private static Logger logger = LogManager.getLogger(App.class.getName());

    /**
     * The main method
     *
     */
    public static void main( String[] args )
    {
        logger.info("start of app");
        Integer[] list = InputArray.getIntegers("Enter up to 10 integer numbers:");
        logger.error("error by entering wrong input");
        Sorting.sorter(list);

    }

}
