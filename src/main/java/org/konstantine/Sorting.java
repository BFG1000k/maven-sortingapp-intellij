package org.konstantine;

import java.util.Arrays;

public class Sorting {
    /**
     * This method is created in purpose of testing usage,
     * @param arr - array to be sorted
     * @return sorted array
     */
    public static Integer[] sorter(Integer[] arr){

        Arrays.sort(arr);

        System.out.println("Sorted list is: ");
        for (int i:arr) {
            System.out.print(i + " ");

        }
        System.out.println();
        return arr;
    }
}
