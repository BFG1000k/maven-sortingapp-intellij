package org.konstantine;

import java.util.ArrayList;
import java.util.Scanner;

public class InputArray {
    /**
     * Method created to accept input from keyboard to console and store it to array
     * @param prompt String message shown to user
     * @return array of integers
     */
     static Integer[] getIntegers(String prompt) {
            System.out.println(prompt);
            Scanner scanner = new Scanner(System.in);

            ArrayList<Integer> list = new ArrayList<Integer>();
            try {
                while (list.size()<10){
                    list.add(Integer.parseInt(scanner.nextLine()));}
            } catch (NumberFormatException e) {

            }
            System.out.println("Number of elements in the list is: " + list.size());
            Integer[] toArray = new Integer[list.size()];
            for (int i = 0; i < list.size(); i++)
                toArray[i] = list.get(i);
            System.out.println("Your numbers are: " + list);
            return toArray;
        }


    }


