package org.konstantine;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.runners.Parameterized.*;

/**
 * parametrized testing class
 */
@RunWith(Parameterized.class)
public class AppTest{


    /**
     * set of parameters:
     * pairs of integer arrays that should be compared in test section
     */
    @Parameters
    public static Collection<Object[]> parameters(){
        return Arrays.asList(new Object[][]{
                {new Integer[] {1,4,1,6,3,5}, new Integer[] {1,1,3,4,5,6}},
                {new Integer[] {11,1,5,16,20,8,3,9,6}, new Integer[] {1,3,5,6,8,9,11,16,20}},
                {new Integer[] {70000,4,1,6,3,5}, new Integer[] {1,3,4,5,6,70000}},
                {new Integer[] {5,1,3,1,1,5}, new Integer[] {1,1,1,3,5,5}},
                {new Integer[] {-42,-33,-100,-7000,-3540,-21000}, new Integer[] {-21000,-7000,-3540,-100,-42,-33}},
                {new Integer[] {20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1}, new Integer[] {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}}

        });
    }

    /**
     * here we explain program that in the above-mentioned pair of arrays the firs array is inputArray and the second array is expectedArray
     */
  @Parameter(0)
  public Integer[] inputArray;
  @Parameter(1)
  public Integer[] expectedArray;

    /**
     * test section
     * we compare expectedArray with input array with implemented method of sorting from our App
     */
  @Test
  public void testArrays(){
      App tester = new App();
       assertArrayEquals(expectedArray,Sorting.sorter(inputArray));

  }




}
